package com.project.ulip;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class TataLifePlan {
	
	String tataDate;
	String tataPrice;
	String tataName = "Tata AIA Life Insurance" ;
	void selectFund(WebDriver driver) {
		Select fund = new Select(driver.findElement(By.id("ddlFund")));
		fund.selectByValue("TGL");
	}
	void selectDay(WebDriver driver) {
		Select day = new Select(driver.findElement(By.id("ddlTimePerioddd")));
		day.selectByValue("30");
	}
	void selectMonth(WebDriver driver) {
		Select month = new Select(driver.findElement(By.id("ddlTimePeriodmm")));
		month.selectByValue("SEP");
	}
	void selectYear(WebDriver driver) {
		Select year = new Select(driver.findElement(By.id("ddlTimePeriodyy")));
		year.selectByValue("2021");
	}
	void clickSubmit(WebDriver driver) {
		driver.findElement(By.cssSelector(".submit_btn.btn_violet")).click();
	}
	void pickDate(WebDriver driver) {
		tataDate = driver.findElement(By.xpath("//div[@class='box-row']/div")).getText();
	}
	void picPrice(WebDriver driver) {
		tataPrice = driver.findElement(By.xpath("//div[@class='box-row']/div[2]")).getText();
	}
	
}
