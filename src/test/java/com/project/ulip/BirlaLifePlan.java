package com.project.ulip;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BirlaLifePlan {
	
	String birlaPrice;
	String birlaDate;
	String birlaName = "Aditya Birla Sun Life Insurance Company Limited" ;
	void pickDate(WebDriver driver) {
		birlaDate = driver.findElement(By.xpath("/html/body/div[3]/div[1]/div/div/div[7]/ul/li[3]/p")).getText();
	}
	void pickPrice(WebDriver driver) {
		birlaPrice = driver.findElement(By.xpath("/html/body/div[3]/div[1]/div/div/div[7]/ul/li[4]/p")).getText();
	}
	
}
