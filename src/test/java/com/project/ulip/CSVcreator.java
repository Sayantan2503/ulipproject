package com.project.ulip;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.opencsv.CSVWriter;

public class CSVcreator {
	String[] aegon, birla, tata;
	public void writeDataLineByLine(String[] aegon, String[] birla, String[] tata)
	{
	    // first create file object for file placed at location
	    // specified by filepath
		this.aegon = aegon;
		this.birla = birla;
		this.tata = tata;
	    File file = new File("E:\\csv outputs\\output.csv");
	    try {
	        // create FileWriter object with file as parameter
	        FileWriter outputfile = new FileWriter(file);
	  
	        // create CSVWriter object filewriter object as parameter
	        CSVWriter writer = new CSVWriter(outputfile);
	  
	        // adding header to csv
	        String[] header = { "Name", "Date", "Price" };
	        writer.writeNext(header);
	  
	        // add data to csv
	        //String[] data1 = { "Aman", "10", "620" };
	        writer.writeNext(aegon);
	       // String[] data2 = { "Suraj", "10", "630" };
	        writer.writeNext(birla);
	        writer.writeNext(tata);
	  
	        // closing writer connection
	        writer.close();
	    }
	    catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	}
}
